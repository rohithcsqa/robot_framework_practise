*** Settings ***
Resource          ../Global/setup.robot

*** Test Cases ***
TC_01_FormSubmission
    LaunchBrowser And Navigate To Application    https://demoqa.com/html-contact-form/    chrome
    Input Text    ${textbox.form.firstname}    rohith
    Input Text    ${textbox.form.lastname}    abc
    Input Text    ${textbox.form.country}    india
    Input Text    ${textbox.form.textarea}    new to robot
    Click Element    ${button.form.submit}
    ${location}    Get Location
    Log    ${location}
    Should Contain    ${location}    onsubmitform
    Log    form submitted successfully.
    Close Browser
