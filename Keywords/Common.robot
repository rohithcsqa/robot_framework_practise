*** Settings ***
Resource          ../Global/setup.robot

*** Keywords ***
LaunchBrowser And Navigate To Application
    [Arguments]    ${url}    ${browser_name}
    Run Keyword If    '${browser_name}'=='chrome' or '${browser_name}' == 'firefox' or '${browser_name}' == 'ie'    Open Browser    ${url}    ${browser_name}
    Maximize Browser Window
    Wait Until Element Is Visible    ${textbox.form.firstname}    ${LONG_WAIT}    FirstName text box is not visible
